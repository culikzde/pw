﻿namespace Reflection
{
    partial class TypeInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TypeInfo));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewReflectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.compileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.leftTabs = new System.Windows.Forms.TabControl();
            this.structureTab = new System.Windows.Forms.TabPage();
            this.structureTree = new System.Windows.Forms.TreeView();
            this.codeTab = new System.Windows.Forms.TabPage();
            this.codeTree = new System.Windows.Forms.TreeView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.centralTabs = new System.Windows.Forms.TabControl();
            this.editorTab = new System.Windows.Forms.TabPage();
            this.editor = new System.Windows.Forms.TextBox();
            this.rightTabs = new System.Windows.Forms.TabControl();
            this.propTab = new System.Windows.Forms.TabPage();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.reflectionTab = new System.Windows.Forms.TabPage();
            this.reflectionGrid = new System.Windows.Forms.DataGridView();
            this.info = new System.Windows.Forms.TextBox();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeTab = new System.Windows.Forms.TabPage();
            this.typeGrid = new System.Windows.Forms.PropertyGrid();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.leftTabs.SuspendLayout();
            this.structureTab.SuspendLayout();
            this.codeTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.centralTabs.SuspendLayout();
            this.editorTab.SuspendLayout();
            this.rightTabs.SuspendLayout();
            this.propTab.SuspendLayout();
            this.reflectionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).BeginInit();
            this.typeTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.compileMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1008, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenuItem,
            this.saveMenuItem,
            this.separator1,
            this.quitMenuItem});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openMenuItem.Text = "&Open";
            this.openMenuItem.Click += new System.EventHandler(this.open_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveMenuItem.Text = "&Save";
            this.saveMenuItem.Click += new System.EventHandler(this.save_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(100, 6);
            // 
            // quitMenuItem
            // 
            this.quitMenuItem.Name = "quitMenuItem";
            this.quitMenuItem.Size = new System.Drawing.Size(103, 22);
            this.quitMenuItem.Text = "&Quit";
            this.quitMenuItem.Click += new System.EventHandler(this.quit_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewReflectionMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "View";
            // 
            // viewReflectionMenuItem
            // 
            this.viewReflectionMenuItem.Name = "viewReflectionMenuItem";
            this.viewReflectionMenuItem.Size = new System.Drawing.Size(127, 22);
            this.viewReflectionMenuItem.Text = "Reflection";
            this.viewReflectionMenuItem.Click += new System.EventHandler(this.viewReflection_Click);
            // 
            // compileMenu
            // 
            this.compileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileMenuItem});
            this.compileMenu.Name = "compileMenu";
            this.compileMenu.Size = new System.Drawing.Size(64, 20);
            this.compileMenu.Text = "Compile";
            // 
            // compileMenuItem
            // 
            this.compileMenuItem.Name = "compileMenuItem";
            this.compileMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.compileMenuItem.Size = new System.Drawing.Size(138, 22);
            this.compileMenuItem.Text = "&Compile";
            this.compileMenuItem.Click += new System.EventHandler(this.compile_Click);
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 739);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1008, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // toolBar
            // 
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(1008, 25);
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "toolStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.info);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 690);
            this.splitContainer1.SplitterDistance = 550;
            this.splitContainer1.TabIndex = 6;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.leftTabs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1008, 550);
            this.splitContainer2.SplitterDistance = 165;
            this.splitContainer2.TabIndex = 5;
            // 
            // leftTabs
            // 
            this.leftTabs.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.leftTabs.Controls.Add(this.structureTab);
            this.leftTabs.Controls.Add(this.codeTab);
            this.leftTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftTabs.Location = new System.Drawing.Point(0, 0);
            this.leftTabs.Multiline = true;
            this.leftTabs.Name = "leftTabs";
            this.leftTabs.SelectedIndex = 0;
            this.leftTabs.Size = new System.Drawing.Size(165, 550);
            this.leftTabs.TabIndex = 6;
            // 
            // structureTab
            // 
            this.structureTab.Controls.Add(this.structureTree);
            this.structureTab.Location = new System.Drawing.Point(23, 4);
            this.structureTab.Name = "structureTab";
            this.structureTab.Padding = new System.Windows.Forms.Padding(3);
            this.structureTab.Size = new System.Drawing.Size(138, 542);
            this.structureTab.TabIndex = 3;
            this.structureTab.Text = "Reflection";
            this.structureTab.UseVisualStyleBackColor = true;
            // 
            // structureTree
            // 
            this.structureTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.structureTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.structureTree.Location = new System.Drawing.Point(3, 3);
            this.structureTree.Name = "structureTree";
            this.structureTree.Size = new System.Drawing.Size(132, 536);
            this.structureTree.TabIndex = 0;
            // 
            // codeTab
            // 
            this.codeTab.Controls.Add(this.codeTree);
            this.codeTab.Location = new System.Drawing.Point(23, 4);
            this.codeTab.Name = "codeTab";
            this.codeTab.Padding = new System.Windows.Forms.Padding(3);
            this.codeTab.Size = new System.Drawing.Size(138, 542);
            this.codeTab.TabIndex = 2;
            this.codeTab.Text = "Code";
            this.codeTab.UseVisualStyleBackColor = true;
            // 
            // codeTree
            // 
            this.codeTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codeTree.Location = new System.Drawing.Point(3, 3);
            this.codeTree.Name = "codeTree";
            this.codeTree.Size = new System.Drawing.Size(132, 536);
            this.codeTree.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.centralTabs);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.rightTabs);
            this.splitContainer3.Size = new System.Drawing.Size(839, 550);
            this.splitContainer3.SplitterDistance = 500;
            this.splitContainer3.TabIndex = 0;
            // 
            // centralTabs
            // 
            this.centralTabs.Controls.Add(this.editorTab);
            this.centralTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centralTabs.Location = new System.Drawing.Point(0, 0);
            this.centralTabs.Name = "centralTabs";
            this.centralTabs.SelectedIndex = 0;
            this.centralTabs.Size = new System.Drawing.Size(500, 550);
            this.centralTabs.TabIndex = 1;
            // 
            // editorTab
            // 
            this.editorTab.Controls.Add(this.editor);
            this.editorTab.Location = new System.Drawing.Point(4, 22);
            this.editorTab.Name = "editorTab";
            this.editorTab.Padding = new System.Windows.Forms.Padding(3);
            this.editorTab.Size = new System.Drawing.Size(492, 524);
            this.editorTab.TabIndex = 1;
            this.editorTab.Text = "Editor";
            this.editorTab.UseVisualStyleBackColor = true;
            // 
            // editor
            // 
            this.editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editor.Location = new System.Drawing.Point(3, 3);
            this.editor.Multiline = true;
            this.editor.Name = "editor";
            this.editor.Size = new System.Drawing.Size(486, 518);
            this.editor.TabIndex = 0;
            this.editor.Text = resources.GetString("editor.Text");
            // 
            // rightTabs
            // 
            this.rightTabs.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.rightTabs.Controls.Add(this.propTab);
            this.rightTabs.Controls.Add(this.reflectionTab);
            this.rightTabs.Controls.Add(this.typeTab);
            this.rightTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightTabs.Location = new System.Drawing.Point(0, 0);
            this.rightTabs.Multiline = true;
            this.rightTabs.Name = "rightTabs";
            this.rightTabs.SelectedIndex = 0;
            this.rightTabs.Size = new System.Drawing.Size(335, 550);
            this.rightTabs.TabIndex = 7;
            // 
            // propTab
            // 
            this.propTab.Controls.Add(this.propGrid);
            this.propTab.Location = new System.Drawing.Point(4, 4);
            this.propTab.Name = "propTab";
            this.propTab.Padding = new System.Windows.Forms.Padding(3);
            this.propTab.Size = new System.Drawing.Size(144, 542);
            this.propTab.TabIndex = 0;
            this.propTab.Text = "Properties";
            this.propTab.UseVisualStyleBackColor = true;
            // 
            // propGrid
            // 
            this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propGrid.Location = new System.Drawing.Point(3, 3);
            this.propGrid.Name = "propGrid";
            this.propGrid.Size = new System.Drawing.Size(138, 536);
            this.propGrid.TabIndex = 6;
            // 
            // reflectionTab
            // 
            this.reflectionTab.Controls.Add(this.reflectionGrid);
            this.reflectionTab.Location = new System.Drawing.Point(4, 4);
            this.reflectionTab.Name = "reflectionTab";
            this.reflectionTab.Padding = new System.Windows.Forms.Padding(3);
            this.reflectionTab.Size = new System.Drawing.Size(144, 542);
            this.reflectionTab.TabIndex = 1;
            this.reflectionTab.Text = "Reflection";
            this.reflectionTab.UseVisualStyleBackColor = true;
            // 
            // reflectionGrid
            // 
            this.reflectionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reflectionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameCol,
            this.valueCol});
            this.reflectionGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reflectionGrid.Location = new System.Drawing.Point(3, 3);
            this.reflectionGrid.Name = "reflectionGrid";
            this.reflectionGrid.Size = new System.Drawing.Size(138, 536);
            this.reflectionGrid.TabIndex = 0;
            // 
            // info
            // 
            this.info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.info.Location = new System.Drawing.Point(0, 0);
            this.info.Multiline = true;
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(1008, 136);
            this.info.TabIndex = 0;
            // 
            // openDialog
            // 
            this.openDialog.FileName = "openFileDialog1";
            // 
            // nameCol
            // 
            this.nameCol.HeaderText = "Name";
            this.nameCol.Name = "nameCol";
            // 
            // valueCol
            // 
            this.valueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valueCol.HeaderText = "Value";
            this.valueCol.Name = "valueCol";
            // 
            // typeTab
            // 
            this.typeTab.Controls.Add(this.typeGrid);
            this.typeTab.Location = new System.Drawing.Point(4, 4);
            this.typeTab.Name = "typeTab";
            this.typeTab.Padding = new System.Windows.Forms.Padding(3);
            this.typeTab.Size = new System.Drawing.Size(308, 542);
            this.typeTab.TabIndex = 2;
            this.typeTab.Text = "Type Info";
            this.typeTab.UseVisualStyleBackColor = true;
            // 
            // typeGrid
            // 
            this.typeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typeGrid.Location = new System.Drawing.Point(3, 3);
            this.typeGrid.Name = "typeGrid";
            this.typeGrid.Size = new System.Drawing.Size(302, 536);
            this.typeGrid.TabIndex = 0;
            // 
            // TypeInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 761);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "TypeInfo";
            this.Text = "Type Info";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.leftTabs.ResumeLayout(false);
            this.structureTab.ResumeLayout(false);
            this.codeTab.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.centralTabs.ResumeLayout(false);
            this.editorTab.ResumeLayout(false);
            this.editorTab.PerformLayout();
            this.rightTabs.ResumeLayout(false);
            this.propTab.ResumeLayout(false);
            this.reflectionTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).EndInit();
            this.typeTab.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.TextBox info;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.TabControl leftTabs;
        private System.Windows.Forms.TabControl centralTabs;
        private System.Windows.Forms.TabPage editorTab;
        private System.Windows.Forms.TabControl rightTabs;
        private System.Windows.Forms.TabPage propTab;
        private System.Windows.Forms.TabPage reflectionTab;
        private System.Windows.Forms.TextBox editor;
        private System.Windows.Forms.ToolStripMenuItem compileMenu;
        private System.Windows.Forms.ToolStripMenuItem compileMenuItem;
        private System.Windows.Forms.TabPage codeTab;
        private System.Windows.Forms.TreeView codeTree;
        private System.Windows.Forms.TabPage structureTab;
        private System.Windows.Forms.TreeView structureTree;
        private System.Windows.Forms.ToolStripMenuItem viewReflectionMenuItem;
        private System.Windows.Forms.DataGridView reflectionGrid;
        private System.Windows.Forms.ToolStripSeparator separator1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueCol;
        private System.Windows.Forms.TabPage typeTab;
        private System.Windows.Forms.PropertyGrid typeGrid;
    }
}

