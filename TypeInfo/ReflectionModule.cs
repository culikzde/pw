﻿using System;
using System.Drawing;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Forms;

namespace Reflection
{
    public class ReflectionModule
    {
        private TreeView reflectionTree;
        private PropertyGrid propGrid;
        private DataGridView reflectionGrid;
        private PropertyGrid typeGrid;

        public ReflectionModule (TreeView reflectionTree0, 
                                 PropertyGrid propGrid0,
                                 DataGridView reflectionGrid0,
                                 PropertyGrid typeGrid0)
        {
            reflectionTree = reflectionTree0;
            propGrid = propGrid0;
            reflectionGrid = reflectionGrid0;
            typeGrid = typeGrid0;
            
            reflectionTree.AfterSelect += reflectionTree_AfterSelect;
        }
        
        public void display (object obj)
        {
            addStructure (reflectionTree.Nodes, obj);
        }
        
        private void addStructure (TreeNodeCollection target, object obj)
        {
           TreeNode node = new TreeNode ();
           node.Tag = obj;
           string typ = obj.GetType().ToString();
           
           if (obj is Control)
           {
              Control c = obj as Control;
              node.Text = c.Name + " : " + typ;
              
              foreach (Control item in c.Controls)
                 addStructure (node.Nodes, item);
           }
           else
           {
              node.Text = obj.ToString () + " : " + typ;
           }

           addAttributes (node.Nodes, obj);
           
           target.Add (node);
        }
        private void addAttributes (TreeNodeCollection target, object obj)
        {
            Type type = obj.GetType ();

            /*
            foreach (Attribute attr in type.GetCustomAttributes ())
            {
                TreeNode node = new TreeNode ();
                node.Tag = attr;
                node.Text = attr.ToString ();
                node.ForeColor = Color.CornflowerBlue;
                target.Add (node);
            }
            */

            foreach (DescriptionAttribute attr in type.GetCustomAttributes<DescriptionAttribute> ())
            {
                TreeNode node = new TreeNode ();
                node.Tag = attr;
                node.Text = attr.Description;
                node.ForeColor = Color.CornflowerBlue;
                target.Add (node);
            }

            /*
            foreach (MethodInfo meth in type.GetRuntimeMethods ())
            {
                if (meth.DeclaringType == type)
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = meth;
                    node.Text = meth.Name;
                    node.ForeColor = Color.Orange;
                    target.Add (node);
                }
            }
            */

            /*
            foreach (MethodInfo meth in type.GetMethods ())
            {
                foreach (Attribute attr in meth.GetCustomAttributes ())
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = attr;
                    node.Text = meth.Name + " : " + attr.ToString ();
                    node.ForeColor = Color.Orange;
                    target.Add (node);
                }
            }
            */

            foreach (MethodInfo meth in type.GetRuntimeMethods ())
            {
                foreach (DescriptionAttribute attr in meth.GetCustomAttributes<DescriptionAttribute> ())
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = attr;
                    node.Text = meth.Name + " : " + attr.Description;
                    node.ForeColor = Color.Orange;
                    target.Add (node);
                }
            }

            foreach (FieldInfo field in type.GetRuntimeFields ())
            {
                var list = field.GetCustomAttributes<DescriptionAttribute> ();
                foreach (DescriptionAttribute attr in list)
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = attr;
                    node.Text = field.Name + " : " + attr.Description;
                    node.ForeColor = Color.Lime;
                    target.Add (node);
                }
            }
        }

        void reflectionTree_AfterSelect (object sender, TreeViewEventArgs e)
        {
           object obj = e.Node.Tag;
           Type typ = obj.GetType ();
           
           propGrid.SelectedObject = obj;
           typeGrid.SelectedObject = obj.GetType ().GetTypeInfo();
           
           foreach (PropertyInfo prop in typ.GetProperties ())
           {
              string name = prop.Name;
              object value = null;
              if (prop.CanRead)
              {
                 value = prop.GetValue (obj, null); /* second parameter required from .Net 4.5 */
              }
              object [] line = new object [] {name, value};
              reflectionGrid.Rows.Add (line);
           }
        }

    } // end of class
} // end of namespace
