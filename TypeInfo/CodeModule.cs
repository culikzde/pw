﻿using System;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.CSharp;

namespace Reflection
 {
    public class CodeModule
    {
        private TreeView codeTree;
        private PropertyGrid propGrid;
        
        public CodeModule (TreeView codeTree0, PropertyGrid propGrid0)
        {
            codeTree = codeTree0;
            propGrid = propGrid0;
            
            codeTree.NodeMouseClick += codeTree_NodeMouseClick;
        }
        
        public void displayAssembly (Assembly a)
        {
            codeTree.Nodes.Clear ();
            foreach (Module m in a.GetModules ())
               displayModule (codeTree.Nodes, m);
        }
        
        private void displayModule (TreeNodeCollection target, Module m)
        {
            TreeNode node = new TreeNode();
            node.Text = "module " + m.Name;
            node.Tag = m;
            foreach (Type t in m.GetTypes ())
                displayType (node, t);
            target.Add (node);
        }

        private void displayType (TreeNode target, Type t)
        {
            TreeNode node = new TreeNode();
            node.Text = "type " + t.Name;
            node.Tag = t;
            foreach (MemberInfo m in t.GetMembers ())
                displayMember (node, m);
            target.Nodes.Add (node);
        }
        
        private void displayMember (TreeNode target, MemberInfo m)
        {
            TreeNode node = new TreeNode();
            node.Text = m.Name;
            node.Tag = m;
            if (m is MethodBase)
            {
                MethodBase b = m as MethodBase;
                foreach (ParameterInfo p in b.GetParameters ())
                    displayParameter (node, p);
            }
            if (m is MethodInfo)
            {
                displayResult (node, m as MethodInfo);
            }
            target.Nodes.Add (node);
        }
        
        private void displayParameter (TreeNode target, ParameterInfo p)
        {
            TreeNode node = new TreeNode();
            node.Text = p.Name + " : " + p.ParameterType.ToString ();
            node.Tag = p;
            target.Nodes.Add (node);
        }
        
        private void displayResult (TreeNode target, MethodInfo m)
        {
            TreeNode node = new TreeNode();
            node.Text = "result " + m.ReturnType.ToString ();
            target.Nodes.Add (node);
        }
        
        private void codeTree_NodeMouseClick (object sender, TreeNodeMouseClickEventArgs e)
        {
            propGrid.SelectedObject = e.Node.Tag;
        }
        
        /* code from stackoverflow.com */

        public void compile (TextBox editor, TextBox info)
        {
            string[] code = { editor.Text };
            
            string[] code0 = {
                                "using System; using System.Data;",
                                "namespace CodeFromFile"+
                                "{"+
                                "   public class CodeFromFile"+
                                "   {"+
                                "       static public int Add(int a ,int b)"+
                                "       {"+
                                "           return a+b;"+
                                "       }"+
                                "   }"+
                                "}"
                         };

               
            CompilerParameters CompilerParams = new CompilerParameters ();

            CompilerParams.GenerateInMemory = true;
            CompilerParams.TreatWarningsAsErrors = false;
            CompilerParams.GenerateExecutable = false;
            CompilerParams.CompilerOptions = "/optimize";
      
            string[] references = { "System.dll", "System.Data.dll" };
            CompilerParams.ReferencedAssemblies.AddRange (references);
      
            CSharpCodeProvider provider = new CSharpCodeProvider ();
            CompilerResults compile = provider.CompileAssemblyFromSource (CompilerParams, code);
      
            if (compile.Errors.HasErrors)
            {
                string text = "Compile error: ";
                foreach (CompilerError ce in compile.Errors)
                {
                    info.AppendText (ce.ToString () + "\r\n");
                    text += "\r\n" + ce.ToString ();
                }
                // throw new Exception (text);
            }
            else
            {
                displayAssembly (compile.CompiledAssembly);
                
                Module module = compile.CompiledAssembly.GetModules ()[0];
                Type mt = null;
                MethodInfo meth = null;
      
                if (module != null)
                    mt = module.GetType ("CodeFromFile.CodeFromFile");
      
                if (mt != null)
                    meth = mt.GetMethod ("Add");
            
                if (meth != null)
                {
                    int result = (int)meth.Invoke (null, new object[] { 5, 10 });
                    info.AppendText ("result = " + result);
                }
            }
       
        }
        
    } // end of class
}// end of namespace
