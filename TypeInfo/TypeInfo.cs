﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Reflection
{
    [Description ("Main Window")]
    public partial class TypeInfo : Form
    {
        [Description ("Compilation")]
        private CodeModule codeModule;

        [Description ("Reflection")]
        private ReflectionModule reflectionModule;

        public TypeInfo ()
        {
            InitializeComponent ();
            codeModule = new CodeModule (codeTree, propGrid);
            reflectionModule = new ReflectionModule (structureTree, propGrid, reflectionGrid, typeGrid);
            viewReflection_Click (null, null);
        }

        [Description ("Open file")]
        private void open_Click (object sender, EventArgs e)
        {
            if (openDialog.ShowDialog () == DialogResult.OK)
            {
                editor.Lines = File.ReadAllLines (openDialog.FileName);
            }
        }

        [Description ("Save file")]
        private void save_Click (object sender, EventArgs e)
        {
            if (saveDialog.ShowDialog () == DialogResult.OK)
            {
                File.WriteAllLines (saveDialog.FileName, editor.Lines);
            }
        } 

        private void quit_Click(object sender , EventArgs e)
        {
            Close ();
        }
        
        /* Reflection */
        
        void viewReflection_Click (object sender, EventArgs e)
        {
           leftTabs.SelectedTab = structureTab;
           rightTabs.SelectedTab = reflectionTab;
           
           structureTree.Nodes.Clear ();
           reflectionModule.display (this);
           structureTree.SelectedNode = structureTree.Nodes[0];
           structureTree.Nodes[0].Expand ();
        }
        
        /* Compilation */
        
        void compile_Click (object sender, EventArgs e)
        {
            leftTabs.SelectedTab = codeTab;
            centralTabs.SelectedTab = editorTab;
            
            info.Clear ();
            codeModule.compile (editor, info);
        }
        
    } // end of class
} // end of namespace
