﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Draw
{
    public class BrushManipulator
    {
        /*
        private Color Color1Field;
        private Color Color2Field;

        private int X1Field = 0;
        private int Y1Field = 0;
        private int X2Field = 100;
        private int Y2Field=  100;
        */

        public BrushManipulator (Color first, Color second)
        {
            Color1 = first;
            Color2 = second;
            
            X1 = 0;
            Y1 = 0;

            X2 = 100;
            Y2 = 100;
        }

        public LinearGradientBrush createBrush ()
        {
            return new LinearGradientBrush (new PointF (X1, Y1),
                                            new PointF (X2, Y2),
                                            Color1,
                                            Color2);
        }

        [Category ("Colors")]
        [DisplayName ("FirstColor")]
        [Description ("First Color ...")]
        public Color Color1 { get; set; }

        [Category ("Colors")]
        [DisplayName ("SecondColor")]
        [Description ("Second Color ...")]
        public Color Color2 { get; set; }

        [Category ("From")]
        public int X1 { get; set; }

        [Category ("From")]
        public int Y1 { get; set; }

        [Category ("To")]
        public int X2 { get; set; }

        [Category ("To")]
        public int Y2 { get; set; }

    }
}
