﻿using System;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Diagram
{
    public partial class ChartArea : UserControl
    {
        public ChartArea (string path)
        {
            InitializeComponent ();
            new Area (this);

            /*
            // http://csharpexamples.com/c-chart-control-example/
            chart.Series.Clear ();
            chart.Titles.Add ("");
            Series data = chart.Series.Add ("first");
            data.Points.Add (10);
            data = chart.Series.Add ("second");
            data.Points.Add (20);
            data = chart.Series.Add ("third");
            data.Points.Add (30);
            */

            /*
            // https://stackoverflow.com/questions/34104484/how-to-build-a-pie-chart-based-on-int-values
            chart.Series.Clear ();
            Series data = chart.Series.Add ("");
            data.ChartType = SeriesChartType.Pie;
            data.Points.AddXY ("first", 10);
            data.Points.AddXY ("second", 20);
            data.Points.AddXY ("third", 30);
            */

            chart.Series.Clear ();
            Series data = chart.Series.Add ("");
            // data.ChartType = SeriesChartType.Pie;

            string[] lines = File.ReadAllLines (path);
            foreach (string line in lines)
            {
                string [] items = line.Split (',');
                int cnt = items.Length;
                string value = (cnt >= 1) ? items[0] : "";
                string name = (cnt >= 2) ? items[1] : "";
                
                int number = 0;
                int.TryParse (value, out number);

                data.Points.AddXY (name, number);
            }

        }

        [ Category ("Data")]
        public Chart Chart { get => chart;  }
    }
}
