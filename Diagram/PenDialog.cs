﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Diagram
{
    public partial class PenDialog : Form
    {
        public PenDialog ()
        {
            InitializeComponent ();
        }

        public void setPen (Pen p)
        {
            propertyGrid.SelectedObject = p;
        }

        public DiagramForm form;

        private void propertyGrid_PropertyValueChanged (object s, PropertyValueChangedEventArgs e)
        {
            if (form != null)
                form.refreshPen ();
        }
    }
}
