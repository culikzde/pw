﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Diagram
{
    public partial class HtmlArea : UserControl
    {
        public HtmlArea (string path)
        {
            InitializeComponent ();
            new Area (this);
            webBrowser.Url = new Uri ("file:///" + path.Replace ('\\', '/').Replace("//", "/"));
        }

        [ Category ("Data") ]
        public WebBrowser Browser { get => webBrowser; }
    }
}
