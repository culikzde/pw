using System;
using System.Drawing;
using System.Windows.Forms;

namespace Diagram
{
    public partial class ImageArea : UserControl
    {
        public ImageArea (string path = "")
        {
            InitializeComponent ();
            new Area (this);

            if (path != "")
                BackgroundImage = new Bitmap (path);
        }

        private void colorMenuItem_Click (object sender, EventArgs e)
        {
            colorDialog.Color = this.BackColor;
            if (colorDialog.ShowDialog () == DialogResult.OK)
                BackColor = colorDialog.Color;
        }

        private void imageMenuItem_Click (object sender, EventArgs e)
        {
            if (openDialog.ShowDialog () == DialogResult.OK)
            {
                BackgroundImage = new Bitmap (openDialog.FileName);
            }
        }

        private void borderMenuItem_Click (object sender, EventArgs e)
        {
            borderMenuItem.Checked = ! borderMenuItem.Checked;
            if (borderMenuItem.Checked)
                BorderStyle = BorderStyle.FixedSingle;
            else
                BorderStyle = BorderStyle.None;
        }

        private void stretchMenuItem_Click (object sender, EventArgs e)
        {
            stretchMenuItem.Checked = ! stretchMenuItem.Checked;
            if (stretchMenuItem.Checked)
                BackgroundImageLayout = ImageLayout.Stretch;
            else
                BackgroundImageLayout = ImageLayout.None;
        }
    }
}