﻿using System.Windows.Forms;

namespace Diagram
{
    public class Area 
    {
        private Control control;

        public Area (Control control0)
        {
            control = control0;

            control.MouseDown += Area_MouseDown;
            control.MouseMove += Area_MouseMove;
            control.MouseUp += Area_MouseUp;
        }

        private int X0, Y0;
        private bool press = false;

        private void Area_MouseDown (object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            press = true;
        }

        private void Area_MouseMove (object sender, MouseEventArgs e)
        {
            if (press)
            {
                if (e.Button == MouseButtons.Left)
                {
                    control.Left += e.X - X0;
                    control.Top += e.Y - Y0;
                }
                else if (e.Button == MouseButtons.Middle)
                {
                    control.Width += e.X - X0;
                    control.Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                }
            }
        }

        private void Area_MouseUp (object sender, MouseEventArgs e)
        {
            press = false;
        }

    } // end of class
} // end of namespace
