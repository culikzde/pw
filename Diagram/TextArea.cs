﻿using System;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

namespace Diagram
{
    public partial class TextArea : UserControl
    {
        public TextArea (string path)
        {
            InitializeComponent ();
            new Area (this);
            textBox.Text = File.ReadAllText (path).Replace ("\n", "\r\n");
        }

        [Category ("Data")]
        public TextBox Editor {  get { return textBox; } }
    }
}
