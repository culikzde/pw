﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

namespace Diagram
{
    public partial class TableArea : UserControl
    {
        public TableArea (string path)
        {
            InitializeComponent ();
            new Area (this);

            string[] lines = File.ReadAllLines (path);
            int columns = 0;
            List< List < string > > complete = new List < List < string > >  ();
            foreach (string line in lines)
            {
                List<string> data = new List<string> ();
                foreach (string item in line.Split (','))
                {
                    data.Add (item);
                }
                int cnt = data.Count;
                if (cnt > columns) columns = cnt;
                complete.Add (data);
            }
            grid.ColumnCount = columns;
            foreach (var data in complete)
                grid.Rows.Add (data.ToArray());
        }

        [ Category ("Data") ]
        public DataGridView Table { get => grid; }
    }
}
