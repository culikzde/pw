﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Diagram
{
    public partial class DiagramForm : Form
    {
        private Pen pen;

        private BrushManipulator manipulator;

        private enum Shape { line, rectangle, ellipse };

        private List <Panel> colorTools = new List <Panel> ();

        public delegate void PenHandler (Pen p);
        public event PenHandler penChanged;

        public delegate void BrushHandler (BrushManipulator b);
        public event BrushHandler brushChanged;

        public DiagramForm ()
        {
            InitializeComponent ();

            colorTools.Add (colorPanel); // modry panel
            addColorPanel (Color.Yellow);
            addColorPanel (Color.Red);
            addColorPanel (Color.Green);

            pen = new Pen (colorTools[0].BackColor); // modra barva podle prvniho panelu

            manipulator = new BrushManipulator (colorTools[1].BackColor, // zluta podle druheho panelu
                                                colorTools[2].BackColor); // cervena podle tretiho panelu

            comboBox.SelectedIndex = (int) Shape.line;

            pictureBox_SizeChanged (null, null); // vytvorime praznou bitmapu

            penChanged += updatePenWidth;

            showFilesMenuItem_Click (null, null);
        }

        private void pictureBox_SizeChanged (object sender, EventArgs e)
        {
            Bitmap old = null;
            if (pictureBox.Image != null)
                old = new Bitmap (pictureBox.Image); // kopie obrazku (pokud existuje)

            int w = pictureBox.Width;
            int h = pictureBox.Height;
            if (old != null)
            {
                // pripadne zvetsime sirku a vysku, at neprijdeme o cast puvodniho obrazku
                if (old.Width > w) w = old.Width;
                if (old.Height > h) h = old.Height;
            }

            Bitmap b = new Bitmap (w, h); 
            Graphics g = Graphics.FromImage (b);
            Brush br = new SolidBrush (Color.White);
            g.FillRectangle (br, 0, 0, w, h); // vyplnime bilou barvou

            if (old != null)
                g.DrawImage (old, 0, 0); // obtiskneme puvodni obrazek

            pictureBox.Image = b;
        }

        /* Pen and Brush */

        private void penWidthNumeric_ValueChanged (object sender, EventArgs e)
        {
            pen.Width = (int)penWidthNumeric.Value;
            refreshPen ();
        }

        private void updatePenWidth (Pen p)
        {
            penWidthNumeric.Value = (int) p.Width;
        }
        public void refreshPen ()
        {
            if (penChanged != null)
                penChanged (pen);
        }
        public void refreshBrush ()
        {
            if (brushChanged != null)
                brushChanged (manipulator);
        }

        /* File Menu */

        private void openMenuItem_Click (object sender, EventArgs e)
        {
            if (openDialog.ShowDialog () == DialogResult.OK)
            {
                pictureBox.Image = new Bitmap (openDialog.FileName);
                pictureBox_SizeChanged (null, null); // zvetsime bitmapu, pokud je obrazek mensi nez okno
            }
        }

        private void saveMenuItem_Click (object sender, EventArgs e)
        {
            // using System.Drawing.Imaging;
            if (saveDialog.ShowDialog () == DialogResult.OK)
                pictureBox.Image.Save (saveDialog.FileName, ImageFormat.Jpeg);
        }

        private void quitMenuItem_Click (object sender, EventArgs e)
        {
            Close ();
        }

        /* Edit Menu */

        private void penMenuItem_Click (object sender, EventArgs e)
        {
            PenDialog dlg = new PenDialog ();
            dlg.setPen (pen);
            penChanged += dlg.setPen;  // dialog bude informovan o zmenach
            dlg.form = this; // dialog bude pomoci form.refreshPen informovat o zmenach
            dlg.Show ();
        }

        private void brushMenuItem_Click (object sender, EventArgs e)
        {
            BrushDialog dlg = new BrushDialog ();
            dlg.setBrushManipulator (manipulator);
            brushChanged += dlg.setBrushManipulator;
            dlg.form = this; 
            dlg.Show ();
        }

        private void addColorMenuItem_Click (object sender, EventArgs e)
        {
            addColorPanel (Color.Orange);
        }

        private void addAreaMenuItem_Click (object sender, EventArgs e)
        {
            ImageArea a = new ImageArea ();
            displayObject (a);
        }

        /* View Menu */

        private void showFilesMenuItem_Click (object sender, EventArgs e)
        {
            leftTabs.SelectedTab = fileTab;
            displayDrives ();
        }

        /* File Tree */

        private void displayDrives ()
        {
            fileTree.Nodes.Clear ();
            DriveInfo[] roots = DriveInfo.GetDrives ();
            foreach (DriveInfo r in roots)
               displayDirectory (fileTree.Nodes, r.RootDirectory);
        }   

        private void displayDirectory (TreeNodeCollection target, DirectoryInfo dir, int levels = 2)
        {
            FileNode node = new FileNode ();
            node.path = dir.FullName;
            node.ready = false;
            node.Text = dir.Name;
            node.ToolTipText = node.path;
            node.ForeColor = Color.Blue;
            target.Add (node);
            displayDetail (node, dir, levels);
        }

        private void displayFile (TreeNodeCollection target, FileInfo f)
        {
            FileNode node = new FileNode ();
            node.path = f.FullName;
            node.ready = true;
            node.Text = f.Name;
            node.ToolTipText = node.path;
            node.ForeColor = Color.Orange;
            target.Add (node);
        }

        private void displayDetail (FileNode node, DirectoryInfo dir, int levels)
        {
            if (levels > 1)
                try
                {
                    foreach (FileInfo f in dir.GetFiles ())
                        displayFile (node.Nodes, f);
                    foreach (DirectoryInfo d in dir.GetDirectories ())
                        displayDirectory (node.Nodes, d, levels - 1);
                    node.ready = true;
                }
                catch (Exception e)
                {
                    TreeNode t = new TreeNode ();
                    t.Text = e.ToString ();
                    t.ToolTipText = e.StackTrace;
                    t.ForeColor = Color.Red;
                    node.Nodes.Add (t);
                }
        }

        private void treeView_BeforeExpand (object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode n in e.Node.Nodes)
            {
                FileNode node = n as FileNode;
                if (node != null && !node.ready)
                {
                    try
                    {
                        DirectoryInfo dir = new DirectoryInfo (node.path);
                        displayDetail (node, dir, 2);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void fileTree_DoubleClick (object sender, EventArgs e)
        {
            TreeNode node = fileTree.SelectedNode as TreeNode;
            if (node != null)
            {
                string path = node.FullPath;
                FileInfo f = new FileInfo (path);
                // String mime = MimeMapping.GetMimeMapping (path);
                // statusLabel.Text = path + ", " + mime;
                string ext = f.Extension;
                statusLabel.Text = path + ", " + ext;
                switch (ext)
                {
                    case ".txt":
                    case ".bat":
                    case ".ini":
                    case ".cs":
                    case ".c":
                    case ".cpp":
                    case ".cc":
                    case ".h":
                    case ".hpp":
                        displayText (path);
                        break;
                    case ".png":
                    case ".bmp":
                    case ".jpg":
                    case ".jpeg":
                        displayImage (path);
                        break;
                    case ".csv":
                        displayTable (path);
                        break;
                    case ".chart":
                        displayChart (path);
                        break;
                    case ".html":
                        displayHtml (path);
                        break;
                }
            }
        }

        public void displayText (string path)
        {
            TextArea a = new TextArea (path);
            displayObject (a);
        }

        public void displayImage (string path)
        {
            ImageArea a = new ImageArea (path);
            displayObject (a);
        }

        public void displayTable (string path)
        {
            TableArea a = new TableArea (path);
            displayObject (a);
        }

        public void displayChart (string path)
        {
            ChartArea a = new ChartArea (path);
            displayObject (a);

        }

        public void displayHtml (string path)
        {
            HtmlArea a = new HtmlArea (path);
            displayObject (a);
        }

        public void displayObject (object obj)
        {
            TreeNode node = new TreeNode ();
            node.Text = obj.ToString ();
            node.Tag = obj;
            tree.Nodes.Add (node);
            if (obj is Control)
            {
                Control c = obj as Control;
                c.Parent = pictureBox;
                c.BringToFront ();
            }
        }

        private void tree_AfterSelect (object sender, TreeViewEventArgs e)
        {
            object obj = e.Node.Tag;
            propGrid.SelectedObject = obj;
            if (obj is Control)
            {
                Control c = obj as Control;
                c.BringToFront ();
            }
        }

            /* Color Panels */

            private void addColorPanel (Color c)
        {
            Panel p = new Panel (); // vytvorime novy panel
            p.BackColor = c;

            // novy panel umistime vpravo od soucasnych panelu
            int cnt = colorTools.Count;
            p.Left = colorPanel.Left + cnt * (colorPanel.Width + colorPanel.Left);

            p.Top = colorPanel.Top; // ze vzoroveho panelu zkopirujeme souradnici y a velikost
            p.Width = colorPanel.Width;
            p.Height = colorPanel.Height;

            p.MouseDown += colorPanel_MouseDown; // pridame reakci na stisknuti mysi

            p.Parent = toolPanel; // pridame do toolPanelu

            colorTools.Add (p);
        }

        private void colorPanel_MouseDown (object sender, MouseEventArgs e)
        {
            Panel panel = sender as Panel;
            // Panel panel = (Panel) sender;

            if (e.Button == MouseButtons.Left && Control.ModifierKeys != Keys.Shift)
            {
                pen.Color = panel.BackColor;
                refreshPen ();
            }
            else if (e.Button == MouseButtons.Right)
            {
                manipulator.Color1 = panel.BackColor;
                manipulator.Color2 = panel.BackColor; // !? jednobarevny stetec
                refreshBrush ();
            }
            else if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Shift)
            {
                colorDialog.Color = panel.BackColor;
                if (colorDialog.ShowDialog () == DialogResult.OK)
                {
                    panel.BackColor = colorDialog.Color;
                    pen.Color = panel.BackColor;
                    refreshPen ();
                }
            }
        }

        /* Picture Box */

        private int X0, Y0;
        private bool press = false;
        private Image save;

        private void pictureBox_MouseDown (object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            press = true;
            save = new Bitmap (pictureBox.Image);
        }

        private void pictureBox_MouseMove (object sender, MouseEventArgs e)
        {
            if (press)
            {
                Graphics g = Graphics.FromImage (pictureBox.Image);
                g.DrawImage (save, 0, 0);

                Brush brush = manipulator.createBrush ();

                Shape inx = (Shape)comboBox.SelectedIndex;
                if (inx == Shape.line)
                {
                    g.DrawLine (pen, X0, Y0, e.X, e.Y);
                }
                else if (inx == Shape.rectangle)
                {
                    int X1 = X0;
                    int Y1 = Y0;
                    int X2 = e.X;
                    int Y2 = e.Y;
                    if (X2 < X1) { int t = X1; X1 = X2; X2 = t; }
                    if (Y2 < Y1) { int t = Y1; Y1 = Y2; Y2 = t; }
                    g.FillRectangle (brush, X1, Y1, X2 - X1, Y2 - Y1);
                    g.DrawRectangle (pen, X1, Y1, X2 - X1, Y2 - Y1);
                }
                else if (inx == Shape.ellipse)
                {
                    g.FillEllipse (brush, X0, Y0, e.X - X0, e.Y - Y0);
                    g.DrawEllipse (pen, X0, Y0, e.X - X0, e.Y - Y0);
                }

                pictureBox.Invalidate ();
            }
        }

        private void pictureBox_MouseUp (object sender, MouseEventArgs e)
        {
            press = false;
        }
    }
}
