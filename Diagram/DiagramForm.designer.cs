﻿namespace Diagram
{
    partial class DiagramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.toolPanel = new System.Windows.Forms.Panel();
            this.penWidthLabel = new System.Windows.Forms.Label();
            this.penWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.colorPanel = new System.Windows.Forms.Panel();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.penMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brushMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addColorMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addAreaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.showFilesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.leftTabs = new System.Windows.Forms.TabControl();
            this.treeTab = new System.Windows.Forms.TabPage();
            this.fileTab = new System.Windows.Forms.TabPage();
            this.fileTree = new System.Windows.Forms.TreeView();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tree = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.toolPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.penWidthNumeric)).BeginInit();
            this.mainMenu.SuspendLayout();
            this.leftTabs.SuspendLayout();
            this.treeTab.SuspendLayout();
            this.fileTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(352, 355);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.SizeChanged += new System.EventHandler(this.pictureBox_SizeChanged);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // toolPanel
            // 
            this.toolPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolPanel.Controls.Add(this.penWidthLabel);
            this.toolPanel.Controls.Add(this.penWidthNumeric);
            this.toolPanel.Controls.Add(this.colorPanel);
            this.toolPanel.Controls.Add(this.comboBox);
            this.toolPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolPanel.Location = new System.Drawing.Point(0, 24);
            this.toolPanel.Name = "toolPanel";
            this.toolPanel.Size = new System.Drawing.Size(869, 33);
            this.toolPanel.TabIndex = 1;
            // 
            // penWidthLabel
            // 
            this.penWidthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.penWidthLabel.AutoSize = true;
            this.penWidthLabel.Location = new System.Drawing.Point(620, 10);
            this.penWidthLabel.Name = "penWidthLabel";
            this.penWidthLabel.Size = new System.Drawing.Size(53, 13);
            this.penWidthLabel.TabIndex = 4;
            this.penWidthLabel.Text = "pen width";
            // 
            // penWidthNumeric
            // 
            this.penWidthNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.penWidthNumeric.Location = new System.Drawing.Point(679, 8);
            this.penWidthNumeric.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.penWidthNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.penWidthNumeric.Name = "penWidthNumeric";
            this.penWidthNumeric.Size = new System.Drawing.Size(41, 20);
            this.penWidthNumeric.TabIndex = 3;
            this.penWidthNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.penWidthNumeric.ValueChanged += new System.EventHandler(this.penWidthNumeric_ValueChanged);
            // 
            // colorPanel
            // 
            this.colorPanel.BackColor = System.Drawing.Color.Blue;
            this.colorPanel.Location = new System.Drawing.Point(4, 4);
            this.colorPanel.Name = "colorPanel";
            this.colorPanel.Size = new System.Drawing.Size(40, 24);
            this.colorPanel.TabIndex = 2;
            this.colorPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.colorPanel_MouseDown);
            // 
            // comboBox
            // 
            this.comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Items.AddRange(new object[] {
            "line",
            "rectangle",
            "ellipse"});
            this.comboBox.Location = new System.Drawing.Point(735, 7);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(121, 21);
            this.comboBox.TabIndex = 0;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(869, 24);
            this.mainMenu.TabIndex = 5;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenuItem,
            this.saveMenuItem,
            this.separator1,
            this.quitMenuItem});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openMenuItem.Text = "&Open";
            this.openMenuItem.Click += new System.EventHandler(this.openMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveMenuItem.Text = "&Save";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(143, 6);
            // 
            // quitMenuItem
            // 
            this.quitMenuItem.Name = "quitMenuItem";
            this.quitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.quitMenuItem.Size = new System.Drawing.Size(146, 22);
            this.quitMenuItem.Text = "&Quit";
            this.quitMenuItem.Click += new System.EventHandler(this.quitMenuItem_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penMenuItem,
            this.brushMenuItem,
            this.addColorMenuItem,
            this.separator2,
            this.addAreaMenuItem});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "&Edit";
            // 
            // penMenuItem
            // 
            this.penMenuItem.Name = "penMenuItem";
            this.penMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.penMenuItem.Size = new System.Drawing.Size(180, 22);
            this.penMenuItem.Text = "&Pen Properties";
            this.penMenuItem.Click += new System.EventHandler(this.penMenuItem_Click);
            // 
            // brushMenuItem
            // 
            this.brushMenuItem.Name = "brushMenuItem";
            this.brushMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.brushMenuItem.Size = new System.Drawing.Size(180, 22);
            this.brushMenuItem.Text = "&Brush Properties";
            this.brushMenuItem.Click += new System.EventHandler(this.brushMenuItem_Click);
            // 
            // addColorMenuItem
            // 
            this.addColorMenuItem.Name = "addColorMenuItem";
            this.addColorMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.addColorMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addColorMenuItem.Text = "add &Color";
            this.addColorMenuItem.Click += new System.EventHandler(this.addColorMenuItem_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(177, 6);
            // 
            // addAreaMenuItem
            // 
            this.addAreaMenuItem.Name = "addAreaMenuItem";
            this.addAreaMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.addAreaMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addAreaMenuItem.Text = "add &Area";
            this.addAreaMenuItem.Click += new System.EventHandler(this.addAreaMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showFilesMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // showFilesMenuItem
            // 
            this.showFilesMenuItem.Name = "showFilesMenuItem";
            this.showFilesMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.showFilesMenuItem.Size = new System.Drawing.Size(153, 22);
            this.showFilesMenuItem.Text = "show &Files";
            this.showFilesMenuItem.Click += new System.EventHandler(this.showFilesMenuItem_Click);
            // 
            // openDialog
            // 
            this.openDialog.FileName = "openFileDialog1";
            // 
            // leftTabs
            // 
            this.leftTabs.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.leftTabs.Controls.Add(this.treeTab);
            this.leftTabs.Controls.Add(this.fileTab);
            this.leftTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftTabs.Location = new System.Drawing.Point(0, 0);
            this.leftTabs.Multiline = true;
            this.leftTabs.Name = "leftTabs";
            this.leftTabs.SelectedIndex = 0;
            this.leftTabs.Size = new System.Drawing.Size(289, 355);
            this.leftTabs.TabIndex = 6;
            // 
            // treeTab
            // 
            this.treeTab.Controls.Add(this.tree);
            this.treeTab.Location = new System.Drawing.Point(23, 4);
            this.treeTab.Name = "treeTab";
            this.treeTab.Padding = new System.Windows.Forms.Padding(3);
            this.treeTab.Size = new System.Drawing.Size(262, 347);
            this.treeTab.TabIndex = 0;
            this.treeTab.Text = "Tree";
            this.treeTab.UseVisualStyleBackColor = true;
            // 
            // fileTab
            // 
            this.fileTab.Controls.Add(this.fileTree);
            this.fileTab.Location = new System.Drawing.Point(23, 4);
            this.fileTab.Name = "fileTab";
            this.fileTab.Padding = new System.Windows.Forms.Padding(3);
            this.fileTab.Size = new System.Drawing.Size(262, 347);
            this.fileTab.TabIndex = 1;
            this.fileTab.Text = "Files";
            this.fileTab.UseVisualStyleBackColor = true;
            // 
            // fileTree
            // 
            this.fileTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileTree.Location = new System.Drawing.Point(3, 3);
            this.fileTree.Name = "fileTree";
            this.fileTree.Size = new System.Drawing.Size(256, 341);
            this.fileTree.TabIndex = 0;
            this.fileTree.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeExpand);
            this.fileTree.DoubleClick += new System.EventHandler(this.fileTree_DoubleClick);
            // 
            // propGrid
            // 
            this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propGrid.Location = new System.Drawing.Point(0, 0);
            this.propGrid.Name = "propGrid";
            this.propGrid.Size = new System.Drawing.Size(220, 355);
            this.propGrid.TabIndex = 7;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 57);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.leftTabs);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(869, 355);
            this.splitContainer1.SplitterDistance = 289;
            this.splitContainer1.TabIndex = 8;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.pictureBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.propGrid);
            this.splitContainer2.Size = new System.Drawing.Size(576, 355);
            this.splitContainer2.SplitterDistance = 352;
            this.splitContainer2.TabIndex = 0;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 390);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(869, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // tree
            // 
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(3, 3);
            this.tree.Name = "tree";
            this.tree.Size = new System.Drawing.Size(256, 341);
            this.tree.TabIndex = 0;
            this.tree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tree_AfterSelect);
            // 
            // DiagramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 412);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolPanel);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "DiagramForm";
            this.Text = "Diagram";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.toolPanel.ResumeLayout(false);
            this.toolPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.penWidthNumeric)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.leftTabs.ResumeLayout(false);
            this.treeTab.ResumeLayout(false);
            this.fileTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.Panel colorPanel;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label penWidthLabel;
        private System.Windows.Forms.NumericUpDown penWidthNumeric;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem addColorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripSeparator separator1;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ToolStripMenuItem penMenuItem;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private System.Windows.Forms.ToolStripMenuItem brushMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAreaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem showFilesMenuItem;
        private System.Windows.Forms.TabControl leftTabs;
        private System.Windows.Forms.TabPage treeTab;
        private System.Windows.Forms.TabPage fileTab;
        private System.Windows.Forms.TreeView fileTree;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.TreeView tree;
    }
}

